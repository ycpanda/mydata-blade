package org.springblade.modules.mydata.job.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import org.apache.commons.text.StringSubstitutor;
import org.springblade.common.util.MdUtil;
import org.springblade.modules.mydata.job.bean.TaskJob;
import org.springblade.modules.mydata.manage.cache.EnvVarCache;
import org.springblade.modules.mydata.manage.entity.Env;
import org.springblade.modules.mydata.manage.entity.EnvVar;
import org.springblade.modules.mydata.manage.service.IEnvService;
import org.springblade.modules.mydata.manage.service.IEnvVarService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 任务的变量处理类
 *
 * @author LIEN
 * @since 2023/11/5
 */
@Component
public class JobVarService {

    @Resource
    private IEnvVarService envVarService;

    @Resource
    private IEnvService envService;

    /**
     * 环境变量 ${env_var}
     */
    private static final String USER_VAR_PATTERN = "\\$\\{([^}]*)\\}";

    /**
     * 系统内置变量 {$sys_var}
     */
    private static final String SYS_VAR_PATTERN = "\\{\\$([^}]*)\\}";

    /**
     * 对接数据属性 {{field}}
     */
    private static final String DATA_FIELD_PATTERN = "\\{\\{([^}]*)\\}\\}";

    /**
     * 仓库已有数据字段 {{$field}}
     */
    private static final String EXISTED_DATA_FIELD_PATTERN = "\\{\\{\\$([^}]*)\\}\\}";

    /**
     * 将json中提取指定数据 保存到任务的指定环境变量
     *
     * @param taskJob    任务
     * @param jsonString json数据
     */
    public void saveVarValue(TaskJob taskJob, String jsonString) {
        if (taskJob == null || StrUtil.isEmpty(jsonString)) {
            return;
        }

        // 接口字段 与 变量的映射
        Map<String, String> fieldVarMapping = taskJob.getFieldVarMapping();
        if (CollUtil.isEmpty(fieldVarMapping)) {
            return;
        }

        JSON json = JSONUtil.parse(jsonString);
        fieldVarMapping.forEach((apiField, varName) -> {
            String varValue = json.getByPath(apiField, String.class);
            Long envId = taskJob.getEnvId();

            EnvVar envVar = new EnvVar();
            envVar.setEnvId(envId);
            envVar.setVarName(varName);
            envVar.setVarValue(varValue);
            envVar.setTenantId(taskJob.getTenantId());

            envVarService.saveByNameInEnv(envVar);
            taskJob.appendLog("保存环境变量，varName：{}，varValue：{}", envVar.getVarName(), envVar.getVarValue());
        });

    }

    /**
     * 从指定map的value中，解析 系统变量、用户环境变量
     *
     * @param map   解析源
     * @param envId 环境id
     */
    public <V> void parseSysAndEnvVar(Map<String, V> map, Long envId) {
        if (CollUtil.isEmpty(map)) {
            return;
        }

        // 替换map中的系统内置变量
        replaceSysVarValues(map);

        // 提取用户自定义变量名
        Map<String, String> userVars = parseEnvVar(map.values(), envId);
        if (MapUtil.isEmpty(userVars)) {
            return;
        }

        replaceUserVarValues(map, userVars);
    }

    /**
     * 从字符串集合中，提取用户环境变量
     *
     * @param strings 字符串集合
     * @param envId   环境id
     * @return 用户环境变量
     */
    public <V> Map<String, String> parseEnvVar(Collection<V> strings, Long envId) {
        // 提取用户自定义变量名
        Set<String> userVarNames = parseUserVarNames(strings);

        // 若没有用户变量名，则结束解析
        if (CollUtil.isEmpty(userVarNames)) {
            return null;
        }

        // 根据变量名 获取环境变量值
        List<EnvVar> envVars = CollUtil.newArrayList();
        Env env = envService.getById(envId);

        // redis中没有缓存 需要查数据库的变量名
        if (env != null && CollUtil.isNotEmpty(userVarNames)) {
            userVarNames.forEach(varName -> {
                // 尝试从redis获取变量
                EnvVar envVar = EnvVarCache.getEnvVar(env.getTenantId(), envId, varName);
                if (envVar != null) {
                    // 替换环境变量值中的系统内置变量
                    envVar.setVarValue(replaceSysVarValue(envVar.getVarValue()));
                    // 环境变量存入返回结果列表
                    envVars.add(envVar);
                }
            });
        }

        // 将环境变量转化为key:value格式
        return envVars.stream().collect(Collectors.toMap(EnvVar::getVarName, EnvVar::getVarValue));
    }

    /**
     * 解析任务API header、param、body 中的系统和环境变量
     *
     * @param taskJob 任务
     */
    public void parseTaskVar(TaskJob taskJob) {

        // 替换 header和param 中的变量
        Map<String, String> reqHeaders = taskJob.getReqHeaders();
        Map<String, Object> reqParams = taskJob.getReqParams();

        parseSysAndEnvVar(reqHeaders, taskJob.getEnvId());
        parseSysAndEnvVar(reqParams, taskJob.getEnvId());

        // 替换 body 中的变量
        if (StrUtil.isNotEmpty(taskJob.getReqBody())) {
            String reqBody = replaceSysVarValue(taskJob.getReqBody());
            Map<String, String> userVars = parseEnvVar(CollUtil.toList(reqBody), taskJob.getEnvId());
            if (MapUtil.isEmpty(userVars)) {
                return;
            }

            reqBody = replaceUserVarValues(reqBody, userVars);
            taskJob.setReqBody(reqBody);
        }
    }

    /**
     * 解析 url、header、param、body 中的 数据字段变量 并替换数据
     *
     * @param taskJob 任务
     */
    public static void parseTaskDataVar(TaskJob taskJob, Map<String, Object> data) {
        if (MapUtil.isEmpty(data)) {
            return;
        }
        // api地址
        String apiUrl = taskJob.getApiUrl();
        // 替换属性变量值
        taskJob.setApiUrl(parseDataFieldVar(apiUrl, data, taskJob.getFieldTypeMapping()));

        // 解析param中的属性变量名
        Map<String, Object> reqParams = taskJob.getReqParams();
        if (MapUtil.isNotEmpty(reqParams)) {
            reqParams.forEach((k, v) -> {
                reqParams.put(k, parseDataFieldVar(StrUtil.toString(v), data, taskJob.getFieldTypeMapping()));
            });
        }

        // 解析header中的属性变量名
        Map<String, String> reqHeaders = taskJob.getReqHeaders();
        if (MapUtil.isNotEmpty(reqHeaders)) {
            reqHeaders.forEach((k, v) -> {
                reqHeaders.put(k, parseDataFieldVar(v, data, taskJob.getFieldTypeMapping()));
            });
        }

        // body
        taskJob.setReqBody(parseDataFieldVar(taskJob.getReqBody(), data, taskJob.getFieldTypeMapping()));
    }

    /**
     * 解析 字符串中{{field}}格式的数据变量
     *
     * @param string 字符串
     * @param data   数据
     * @return 解析后的字符串
     */
    public static String parseDataFieldVar(String string, Map<String, Object> data, Map<String, String> fieldTypeMapping) {
        return parseDataVar(string, data, fieldTypeMapping, DATA_FIELD_PATTERN, "{{", "}}");
    }

    /**
     * 字符串是否为 属性表达式 {{field}}
     *
     * @param string 字符串
     * @return true-是属性表达式，false-不是
     */
    public static boolean isFieldExp(String string) {
        return ReUtil.isMatch(DATA_FIELD_PATTERN, string);
    }

    /**
     * 解析 字符串中 {{$field}} 格式的数据变量
     *
     * @param string 字符串
     * @param data   数据
     * @return 解析后的字符串
     */
    public static String parseExistedDataVar(String string, Map<String, Object> data, Map<String, String> fieldTypeMapping) {
        return parseDataVar(string, data, fieldTypeMapping, EXISTED_DATA_FIELD_PATTERN, "{{$", "}}");
    }

    private static String parseDataVar(String string, Map<String, Object> data, Map<String, String> fieldTypeMapping, String pattern, String prefix, String suffix) {
        if (StrUtil.isEmpty(string)) {
            return string;
        }
        // 解析字符串中的属性变量名 {{field}}
        List<String> fieldNames = parseVarNames(string, pattern, prefix, suffix);
        // 若解析为空，则结束
        if (CollUtil.isEmpty(fieldNames)) {
            return string;
        }
        // 替换映射
        Map<String, String> replaceMap = MapUtil.newHashMap();
        for (String field : fieldNames) {
            // 尝试获取数据的类型，若没有则默认为字符串
            String targetType = fieldTypeMapping.get(field);

            String value = "";
            if (MapUtil.isEmpty(data) || !data.containsKey(field)) {
                value = MdUtil.defaultValue(targetType);
            } else {
                // 从数据中 取出数据 并存入替换映射
                value = MdUtil.formatData(data.get(field), targetType);
            }
            replaceMap.put(field, value);
        }

        StringSubstitutor stringSubstitutor = new StringSubstitutor(replaceMap);
        stringSubstitutor.setVariablePrefix(prefix);
        stringSubstitutor.setVariableSuffix(suffix);
        // 替换变量值
        return stringSubstitutor.replace(string);
    }

    /**
     * 替换用户自定义变量 ${var}
     *
     * @param sourceMap 替换前的map数据
     * @param varMap    变量名-变量值
     */
    private <V> void replaceUserVarValues(Map<String, V> sourceMap, Map<String, String> varMap) {
        StringSubstitutor stringSubstitutor = new StringSubstitutor(varMap);
        sourceMap.forEach((k, v) -> {
            // 替换用户自定义变量
            sourceMap.put(k, (V) stringSubstitutor.replace(v));
        });
    }

    /**
     * 替换用户自定义变量 ${var}
     *
     * @param string 字符串
     * @param varMap 变量名-变量值
     */
    private String replaceUserVarValues(String string, Map<String, String> varMap) {
        StringSubstitutor stringSubstitutor = new StringSubstitutor(varMap);
        // 替换用户自定义变量
        return stringSubstitutor.replace(string);
    }

    /**
     * 从多个字符串中 解析所有${}表达式中的变量名
     *
     * @param strings 字符串集合
     * @return 变量名列表
     */
    private Set<String> parseUserVarNames(Collection<?> strings) {
        Set<String> userVarNames = CollUtil.newHashSet();
        if (CollUtil.isNotEmpty(strings)) {
            for (Object string : strings) {
                userVarNames.addAll(parseVarNames(string.toString(), USER_VAR_PATTERN, "${", "}"));
            }
        }
        return userVarNames;
    }

    /**
     * 解析处理map值中的系统内置变量
     *
     * @param map Map对象
     * @param <V> 值类型
     */
    private <V> void replaceSysVarValues(Map<String, V> map) {
        if (MapUtil.isEmpty(map)) {
            return;
        }

        map.forEach((k, v) -> {
            // 替换用户自定义变量
            map.put(k, (V) replaceSysVarValue(v.toString()));
        });
    }

    /**
     * 解析处理系统内置变量 {$var}
     *
     * @param string 被解析的字符串
     * @return 替换后的字符串
     */
    private String replaceSysVarValue(String string) {
        // 解析系统内置变量
        List<String> sysVarNames = parseVarNames(string, SYS_VAR_PATTERN, "{$", "}");
        if (CollUtil.isEmpty(sysVarNames)) {
            return string;
        }

        Map<String, String> replaceMap = MapUtil.newHashMap();
        for (String sysVarName : sysVarNames) {
            switch (sysVarName) {
                case "timestamp":
                    long timestamp = DateUtil.current();
                    replaceMap.put("timestamp", String.valueOf(timestamp));
                    break;
                case "timestamp_second":
                    long second = DateUtil.currentSeconds();
                    replaceMap.put("timestamp_second", String.valueOf(second));
                    break;
            }
        }

        StringSubstitutor stringSubstitutor = new StringSubstitutor(replaceMap);
        stringSubstitutor.setVariablePrefix("{$");
        stringSubstitutor.setVariableSuffix("}");
        return stringSubstitutor.replace(string);
    }

    /**
     * 从字符串中 解析指定表达式中的变量名
     *
     * @param string  字符串
     * @param pattern 表达式
     * @return 变量名列表
     */
    public static List<String> parseVarNames(String string, String pattern, String prefix, String suffix) {
        if (StrUtil.isEmpty(string)) {
            return CollUtil.newArrayList();
        }

        List<String> varNames = ReUtil.findAll(pattern, string, 0);
        if (CollUtil.isNotEmpty(varNames)) {
            ListIterator<String> iterator = varNames.listIterator();
            while (iterator.hasNext()) {
                String varName = iterator.next();
                varName = getKey(varName, prefix.length(), suffix.length());
                iterator.set(varName);
            }
        }
        return varNames;
    }

    private static String getKey(String g, int prefix, int suffix) {
        return g.substring(prefix, g.length() - suffix);
    }
}
