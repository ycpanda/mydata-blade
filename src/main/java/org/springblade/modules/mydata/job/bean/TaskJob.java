package org.springblade.modules.mydata.job.bean;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springblade.common.constant.MdConstant;
import org.springblade.common.util.MapUtil;
import org.springblade.modules.mydata.data.BizDataFilter;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 包含执行接口的 任务对象
 *
 * @author LIEN
 * @since 2022/07/14
 */
@Data
@EqualsAndHashCode(of = "id")
public class TaskJob implements Serializable {

    private static final long serialVersionUID = 1L;

    // ----- 任务相关信息 -----
    private Long id;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 所属环境
     */
    private Long envId;

    /**
     * 任务周期
     */
    private String taskPeriod;

    /**
     * 操作类型
     *
     * @see MdConstant#DATA_PRODUCER
     * @see MdConstant#DATA_CONSUMER
     */
    private Integer opType;

    /**
     * 待执行次数，默认Integer.MAX_VALUE，0 结束，正整数 待执行数
     */
    private int times = MdConstant.TASK_JOB_DEFAULT_TIMES;

    // ----- 接口相关信息 -----

    /**
     * 接口method
     */
    private String apiMethod;

    /**
     * 接口地址
     */
    private String apiUrl;

    /**
     * 接口类型：json
     */
    private String dataType;

    /**
     * 接口请求Header
     */
    private Map<String, String> reqHeaders;

    /**
     * 原始的接口请求Header
     */
    private Map<String, String> originReqHeaders;

    /**
     * 接口请求参数
     */
    private Map<String, Object> reqParams;

    /**
     * 原始的接口请求参数
     */
    private Map<String, Object> originReqParams;

    /**
     * 接口字段与变量名的映射
     */
    private Map<String, String> fieldVarMapping;

    // ----- 数据相关信息 -----

    /**
     * 字段层级前缀
     */
    private String apiFieldPrefix;

    /**
     * 字段映射配置
     */
    private LinkedHashMap<String, String> fieldMapping;

    /**
     * 所属租户
     */
    private String tenantId;

    /**
     * 所属数据项id
     */
    private Long dataId;

    /**
     * 所属数据编号
     */
    private String dataCode;

    /**
     * 唯一标识字段编号
     */
    private String idFieldCode;

    /**
     * 数据的过滤条件
     */
    private List<BizDataFilter> dataFilters;

    // ----- 定时任务相关信息 -----

    /**
     * 下一次执行时间
     */
    private Date nextRunTime = new Date();

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 执行时间
     */
    private Date lastRunTime;

    /**
     * 最后成功时间
     */
    private Date lastSuccessTime;

    /**
     * 日志记录
     */
    private StringBuffer log = new StringBuffer();

    /**
     * 执行结果，0-失败，1-成功
     */
    private Integer executeResult;

    /**
     * 任务执行次数，记录重试次数
     */
    private int executeCount = 0;

    /**
     * 是否为订阅任务：0-不订阅，1-订阅
     */
    private Integer isSubscribed;

    /**
     * 接口返回的数据
     */
    private List<Map<String, Object>> produceDataList;

    /**
     * 待消费的数据
     */
    private List<Map<String, Object>> consumeDataList;

    /**
     * 被过滤的无效数据
     */
    private List<Map<String, Object>> filteredDataList;

    /**
     * 所属项目id
     */
    private Long projectId;

    /**
     * 分批启用状态：0-不启用，1-启用
     */
    private boolean isBatch;

    /**
     * 分批间隔（秒）
     */
    private Integer batchInterval;

    /**
     * 分批参数
     */
    private List<TaskBatchParam> batchParams;

    /**
     * 原始分批参数
     */
    private List<TaskBatchParam> originBatchParams;

    /**
     * 分批数量
     */
    private Integer batchSize;

    /**
     * 配置映射的数据字段的类型
     */
    private Map<String, String> fieldTypeMapping;

    /**
     * 字段默认值
     */
    private Map<String, String> fieldDefaultValues;

    /**
     * 提供数据模式，默认1，1-API、2-接收推送
     */
    private Integer produceMode;

    /**
     * 消费数据模式，默认1，1-API、2-发邮件
     */
    private Integer consumeMode;

    /**
     * 消费数据模式的收件人邮件
     */
    private String consumeEmail;

    /**
     * 跳过特殊情况
     */
    private Integer skipError;

    /**
     * 任务是否失败
     */
    private boolean isFailed = false;

    /**
     * 任务创建者id
     */
    private Long createUser;

    /**
     * 新增数据总量
     */
    private int insertCount;

    /**
     * 更新数据总理
     */
    private int updateCount;

    /**
     * 消费数据总量
     */
    private int consumeCount;

    /**
     * 接收到的数据
     */
    private String acceptedData;

    /**
     * 单条记录消费模式，1-对象、2-集合
     */
    private Integer dataMode;

    /**
     * 数据批次标识
     */
    private String dataBatchId;

    /**
     * 任务日志
     */
    private Long taskLogId;

    /**
     * 任务内置变量
     */
    private Map<String, Object> taskVar = MapUtil.newHashMap();

    /**
     * 数据处理配置
     */
    private Map<String, Map<String, String>> dataProcess;

    /**
     * 请求体
     */
    private String reqBody;

    /**
     * 是否手动执行的临时任务
     */
    private boolean isTemp;

    /**
     * 任务原来的状态，以便手动执行后 恢复为原来状态
     */
    private Integer oldTaskStatus;

    /**
     * 是否清除HTML标签，0-不清除、1-清除
     */
    private Integer cleanHtml;

    /**
     * 是否启用历史记录：0-不启用、1-启用
     */
    private Integer enableHistory;

    /**
     * 追加日志
     */
    public void appendLog(String log, Object... params) {
        String template = DateUtil.format(new Date(), DatePattern.NORM_DATETIME_MS_PATTERN) + " - " + log;
        String newLog = StrUtil.format(template, params);
        this.log.append(newLog).append("\n");
    }
}
