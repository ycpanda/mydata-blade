package org.springblade.modules.mydata.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.modules.mydata.manage.entity.BizData;

/**
 * 业务数据概况 Mapper接口
 *
 * @author LIEN
 * @since 2024/4/26
 */
public interface BizDataMapper extends BaseMapper<BizData> {
}
